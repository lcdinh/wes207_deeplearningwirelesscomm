// Last 2 fully connected layers
// Auto-Decoder

// Dense Layer 3
#define L3_Din 7		// Input Nodes
#define L3_Dout 16		// Output Nodes
#define L3_Ibit 8		// Quantized weights
#define L3_Wbit 8		// Quantized bias
#define L3_Mbit 32		// Multiply bits
#define L3_Abit 32		// Activation bits
#define L3_InP 32		// Input Precision
#define L3_OutP 32		// Output Precision

// Dense Layer 4
#define L4_Din 16
#define L4_Dout 16
#define L4_Ibit 8
#define L4_Wbit 8
#define L4_Mbit 32
#define L4_Abit 32
#define L4_InP 32
#define L4_OutP 32

#define SCALE_BITS 18
#define FACTOR_SCALE_BITS 22
#define HIGH_PREC_SCALE_BITS 22