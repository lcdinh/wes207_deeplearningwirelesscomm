#define AP_INT_MAX_W 16384

#include "hls-nn-lib.h"
#include "auto-decoder-config.h"
#include "auto-decoder-param.h"

void DoCompute(stream<ap_axis >& in, stream<ap_axis >& out, const unsigned int numReps) {
#pragma HLS INTERFACE axis register both port=out
#pragma HLS INTERFACE axis register both port=in
#pragma HLS INTERFACE s_axilite port=numReps bundle=control
#pragma HLS INTERFACE s_axilite port=return bundle=control

// Interface to store weights, bias and activation function
#pragma HLS RESOURCE variable=weights3 core=RAM_1P_BRAM
#pragma HLS ARRAY_PARTITION variable=weights3 complete dim=0
#pragma HLS RESOURCE variable=A3 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=bias3 core=RAM_1P_BRAM
#pragma HLS ARRAY_PARTITION variable=A3 complete dim=0
#pragma HLS ARRAY_PARTITION variable=bias3 complete dim=0
#pragma HLS RESOURCE variable=weights4 core=RAM_1P_BRAM
#pragma HLS ARRAY_PARTITION variable=weights4 complete dim=0
#pragma HLS RESOURCE variable=A4 core=RAM_1P_BRAM
#pragma HLS RESOURCE variable=bias4 core=RAM_1P_BRAM
#pragma HLS ARRAY_PARTITION variable=A4 complete dim=0
#pragma HLS ARRAY_PARTITION variable=bias4 complete dim=0

#pragma HLS DATAFLOW
	// Stream input vector
	stream<ap_uint<L3_Din*L3_InP> > in_stream("in_stream");

	// Layer 3
	stream<ap_uint<L3_OutP*L3_Abit> > dense3("dense3");
	DENSE_ACT<L3_Din, L3_Dout, L3_Ibit, L3_Wbit, L3_Mbit, L3_Abit, L3_InP, L3_OutP, SCALE_BITS, FACTOR_SCALE_BITS>
	(	in_stream, weights3, factorA3, factorB3, dense3, numReps);

	// Layer 4
	stream<ap_uint<L5_OutP*L5_Mbit> > dense4("dense4");
	DENSE_NOACT<L4_Din, L4_Dout, L4_Ibit, L4_Wbit, L4_Mbit, L4_InP, L4_OutP, SCALE_BITS>
	(dense3, weights4, factorA4, factorB4, dense4, numReps);

	// Output layer
	stream<ap_uint<16> > out_nolast("out_nolast");

	AddLast<1>(out_nolast, out, numReps);
}

#include <iostream>

using namespace std;

int main(int argc, char* argv[]) {
#define NUM_SAMPLES 50000

	const unsigned int buffer_size = NUM_SAMPLES;
	stream<ap_axis > inputStream;
	
	unsigned int index = 0;

	// Generate some random one hot vector just to test the hardware implmentation
	// TODO: to generate meaningful data needs to write a testbench!
	for (unsigned int i = 0; i < buffer_size; i++) {
		ap_axis temp;
		temp.data(i) = i%16;
		cout << "inputStream[" << i << "]: " << hex << temp.data << dec << endl;
		inputStream.write(temp);
	}
	
	stream<ap_axis > outputStream;

	// Inference network
	DoCompute(inputStream, outputStream, NUM_SAMPLES);


	ap_axis outputBuffer[NUM_SAMPLES];

	for (unsigned int i = 0; i < NUM_SAMPLES; i++) {
		outputBuffer[i] = outputStream.read();
	}

}