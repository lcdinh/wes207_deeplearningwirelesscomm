function [out] = quantize_w(x, bitW)
%QUANTIZE_W Summary of this function goes here
%   Detailed explanation goes here
x1 = tanh(x);
x2 = x1 / max(max(abs(x1))) * 0.5 + 0.5;
x3 = quantize(x2, bitW);
out = 2 * x3 - 1;
end

