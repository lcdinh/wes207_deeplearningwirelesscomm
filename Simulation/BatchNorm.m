function [y] = BatchNorm(x, mean, std, gamma, beta)
%BATCHNORM Summary of this function goes here
%   Detailed explanation goes here
x_norm = (x - mean)./sqrt(std + 1e-8);
y = gamma .* x_norm + beta;
end

