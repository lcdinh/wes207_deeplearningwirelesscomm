close all;
clearvars;

vec = eye(16);
a = [   0 0 0 0;
        0 0 0 1;
        0 0 1 0;
        0 0 1 1;
        0 1 0 0;
        0 1 0 1;
        0 1 1 0;
        0 1 1 1;
        1 0 0 0;
        1 0 0 1;
        1 0 1 0;
        1 0 1 1;
        1 1 0 0;
        1 1 0 1;
        1 1 1 0;
        1 1 1 1];
    
[ind,n] = vec2ind(vec);
ind
n
vec2 = full(ind2vec(ind,n));