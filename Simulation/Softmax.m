function [y] = Softmax(x)
%RELU Summary of this function goes here
%   Detailed explanation goes here
% y = exp(x)/sum(exp(x),2);
y = exp(x)./repmat(sum(exp(x),2),1,16);
end

