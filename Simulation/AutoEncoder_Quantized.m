close all;
clearvars;

%% Load all weights
load('l1_b.mat');
load('l1_w.mat');

load('l2_b.mat');
load('l2_w.mat');

load('l3_beta.mat');
load('l3_gama.mat');
load('l3_mean.mat');
load('l3_std.mat');

load('l4_b.mat');
load('l4_w.mat');

load('l5_b.mat');
load('l5_w.mat');

% Quantization
bitW = 16;
bitA = 32;

l1_b = quantize_w(l1_b, bitW);
l1_w = quantize_w(l1_w, bitW);

l2_b = quantize_w(l2_b, bitW);
l2_w = quantize_w(l2_w, bitW);

l4_b = quantize_w(l4_b, bitW);
l4_w = quantize_w(l4_w, bitW);

l5_b = quantize_w(l5_b, bitW);
l5_w = quantize_w(l5_w, bitW);
%% 
N = 4*10^6;% number of bits 

Eb_N0_dB = [0:1:10]; % multiple Eb/N0 values
Ec_N0_dB = Eb_N0_dB - 10*log10(7/4); % code block energy/ Noise

for yy = 1:length(Eb_N0_dB)

   %% Transmitter
   ip = rand(1,N)>0.5; % generating 0,1 with equal probability
   
   %% One hot encoding
   % 4-bits message slice
   ipM = reshape(ip,4,N/4).';
   % 16-bits codeword from one hot encoding 
   ipM_dec = sum(ipM.*kron(ones(N/4,1),[8 4 2 1]),2);   % Convert 4 bits to Dec
   vec2 = (full(ind2vec((ipM_dec+1).',16))).';          % Decimal to 1 hot vec
   
   
   % Layer 1 - ReLu
   l1 = vec2*l1_w + l1_b;
   l1 = ReLu(l1);
%    l1 = quantize(l1, bitA);
   
   % Layer 2 - Linear
   l2 = l1*l2_w + l2_b;
%    l2 = quantize(l2, bitA);
   
   % Layer 3 - Batch Normalization
   l3  = BatchNorm(l2, l3_mean, l3_std, l3_gama, l3_beta);
%    l3 = quantize(l3, bitA);
   encoder = l3;
   
   % Coderwords after encoder
   ipC = vec2;
   ipC = encoder;
   % convert back to 1D bit-stream
   cip = reshape(ipC.',1,N/4*7);
   
   %% Modulation there's no modulation
   s = cip;

   %% Channel - AWGN
   % Noise addition (Accurate model)
   n = 1/sqrt(2)*[randn(size(cip)) + j*randn(size(cip))]; % white gaussian noise, 0dB variance 
   y = s + 10^(-Ec_N0_dB(yy)/20)*n; % additive white gaussian noise
   
   %% Receiver 
   cipHard = real(y); % BPSK decode real part only

   %% Auto-Decoder
   cipHardM    = reshape(cipHard,7,N/4).';
   dl4= cipHardM*l4_w + l4_b;
   dl4 = ReLu(dl4);
%    dl4 = quantize(dl4, bitA);
   
   dl5= dl4*l5_w + l5_b;
   dl5 = Softmax(dl5);
%    dl5 = quantize(dl5, bitA);
   [maxval,maxidx] = max(dl5,[],2);
   ipHat = maxidx - 1;
   comp = [ipM_dec ipHat];
  
   % counting the errors
   nErr_auto(yy) = numel(find(ipM_dec~=ipHat));
end
theoryBer = 0.5*erfc(sqrt(10.^(Eb_N0_dB/10))); % theoretical ber uncoded AWGN
simBer_auto    = nErr_auto/N;
load('simBer_hard.mat');
load('simBer_soft.mat');
figure;
semilogy(Eb_N0_dB(1:10),theoryBer(1:10),'bd-','LineWidth',2);
hold on
semilogy(Eb_N0_dB(1:10),simBer_hard(1:10),'ms-','LineWidth',2);
semilogy(Eb_N0_dB(1:10),simBer_soft(1:10),'rp-','LineWidth',2);
semilogy(Eb_N0_dB(1:10),simBer_auto(1:10),'gp-','LineWidth',2);
set(gca,'FontSize',20);
grid on
legend('theory - uncoded', 'Hamming 7,4 (hard)','Hamming 7,4 (soft)', 'Auto-encoder (Post-Quantization)');
xlabel('Eb/No, dB');
ylabel('Bit Error Rate');
title('BER for BPSK in AWGN with Auto-encoder');